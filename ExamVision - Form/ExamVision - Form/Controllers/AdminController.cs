﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExamVision___Form.Models;
using ExamVision___Form.ModelsView;
using Microsoft.AspNet.Identity;

namespace ExamVision___Form.Controllers
{
    public class AdminController : Controller
    {
        [Authorize(Roles = "Admin")]
        public ActionResult AdminPanel()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            AdminPanelViewModel model = new AdminPanelViewModel();

            model.Cosmo = db.Glasses.FirstOrDefault(x => x.Name == "Cosmo");
            model.Sport = db.Glasses.FirstOrDefault(x => x.Name == "Sport");
            model.Icon = db.Glasses.FirstOrDefault(x => x.Name == "Icon");
            model.Essential = db.Glasses.FirstOrDefault(x => x.Name == "Essential");

            model.Completed = db.FormModels.Where(x => x.isOrdered == true).OrderByDescending(x => x.ImportDate).ToList();

            return View(model);
        }

        public ActionResult UpdateInventory(AdminPanelViewModel model)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            db.Glasses.AddOrUpdate(model.Cosmo);
            db.Glasses.AddOrUpdate(model.Sport);
            db.Glasses.AddOrUpdate(model.Icon);
            db.Glasses.AddOrUpdate(model.Essential);

            //db.Entry(model.Cosmo).State = EntityState.Modified;
            //db.Entry(model.Sport).State = EntityState.Modified;
            //db.Entry(model.Icon).State = EntityState.Modified;
            //db.Entry(model.Essential).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("AdminPanel");
        }


        [Authorize]
        public ActionResult UserPanel()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            UserPanelViewModel model = new UserPanelViewModel();

            string uid = User.Identity.GetUserId();
            model.Incomplete = db.FormModels.Where(x => x.isOrdered == false).Where(x => x.User.Id == uid).OrderByDescending(x => x.ImportDate).ToList();
            model.Completed = db.FormModels.Where(x => x.isOrdered == true).Where(x => x.User.Id == uid).OrderByDescending(x => x.ImportDate).ToList();

            return View(model);
        }
    }
}