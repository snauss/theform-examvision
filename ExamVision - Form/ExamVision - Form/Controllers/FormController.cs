﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExamVision___Form.Models;
using ExamVision___Form.ModelsView;
using ExamVision___Form.TransferMethod;
using Microsoft.AspNet.Identity;

namespace ExamVision___Form.Controllers
{
    public class FormController : Controller
    {

        // GET: Form
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            var userId = User.Identity.GetUserId();
            var user = db.Users.SingleOrDefault(x => x.Id == userId);
            //Moved to HomeController
            ViewBag.userMail = user.Email;

            return View(db.FormModels.ToList());
        }

        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        // GET: Form/Details/5
        public ActionResult Details(int? id)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormModel formModel = db.FormModels.Find(id);
            if (formModel == null)
            {
                return HttpNotFound();
            }
            return PartialView("_Details", formModel);
        }

        // GET: Form/Create
        //public ActionResult Create()
        //{
        //    ApplicationDbContext db = new ApplicationDbContext();
        //    ViewBag.Cosmo = db.Glasses.SingleOrDefault(x => x.Name == "Cosmo");
        //    ViewBag.Sport = db.Glasses.SingleOrDefault(x => x.Name == "Sport");
        //    ViewBag.Icon = db.Glasses.SingleOrDefault(x => x.Name == "Icon");
        //    ViewBag.Essential = db.Glasses.SingleOrDefault(x => x.Name == "Essential");

        //    return View();
        //}

        // POST: Form/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[Authorize]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(FormModel formModel)
        //{
            
        //    ApplicationDbContext db = new ApplicationDbContext();

        //    formModel.ImportDate = DateTime.Now;
        //    if (ModelState.IsValid)
        //    {
        //        db.FormModels.Add(formModel);
        //        db.SaveChanges();
        //        return RedirectToAction("EditConfirmation");
        //    }

        //    return View(formModel);
        //}

        // GET: Form/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            ViewBag.Cosmo = db.Glasses.SingleOrDefault(x => x.Name == "Cosmo");
            ViewBag.Sport = db.Glasses.SingleOrDefault(x => x.Name == "Sport");
            ViewBag.Icon = db.Glasses.SingleOrDefault(x => x.Name == "Icon");
            ViewBag.Essential = db.Glasses.SingleOrDefault(x => x.Name == "Essential");

            var userId = User.Identity.GetUserId();
            var user = db.Users.SingleOrDefault(x => x.Id == userId);
            ViewBag.userMail = user.Email;

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //FormModel formModel = db.FormModels.Find(id);
            FormViewModel model = new FormViewModel();
            model.Form = db.FormModels.SingleOrDefault(x => x.ID == id);
            if (model.Form == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: Form/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FormViewModel formModel)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            if (ModelState.IsValid)
            {
                if (formModel.Form.isOrdered == false)
                {
                    // uploaded file
                    if (formModel.UploadedFile != null && formModel.UploadedFile.ContentLength > 0)
                    {
                        // Remove old picture
                        try
                        {
                            System.IO.File.Delete(formModel.Form.picture_upload_data_field);
                        }
                        catch (Exception)
                        {

                        }
                        string directory = Server.MapPath("~/Content/ClientPics/");
                        string filename = "ClientImg_" + formModel.Form.ID +
                                          Path.GetExtension(formModel.UploadedFile.FileName);
                        var path = Path.Combine(directory, filename);

                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);

                        formModel.UploadedFile.SaveAs(path);
                        formModel.Form.picture_upload_data_field = "/Content/ClientPics/" + filename;
                    }
                    // Save
                    db.FormModels.AddOrUpdate(formModel.Form);
                    db.SaveChanges();
                }
                return RedirectToAction("EditConfirmation", new {id = formModel.Form.ID});
            }
            return View(formModel);
        }

       
        public ActionResult EditConfirmation(int? id)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            FormModel model = db.FormModels.SingleOrDefault(x => x.ID == id);
            return View(model);
        }


        // GET: Form/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormModel formModel = db.FormModels.Find(id);
            if (formModel == null)
            {
                return HttpNotFound();
            }
            return View(formModel);
        }

        // POST: Form/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int? id)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            FormModel formModel = db.FormModels.Find(id);
            if (formModel.isOrdered == false)
            {
                db.FormModels.Remove(formModel);
                db.SaveChanges();
            }
            return RedirectToAction("UserPanel", "Admin");
        }

        protected override void Dispose(bool disposing)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [Authorize]
        public ActionResult OrderCompletion(int? id)
        {
            ApplicationDbContext db = new ApplicationDbContext();

            FormModel form = db.FormModels.Find(id);
            

            //Donez by le tvede
            //Aktiver det her 
            var mysqlServer = new MysqlContext();
            mysqlServer.Database.CreateIfNotExists();
            mysqlServer.SaveChanges();
            var exportModel = TransferConverter.Convert(form);
            mysqlServer.test_orders.Add(exportModel);
            mysqlServer.SaveChanges();

            form.isOrdered = true;
            db.SaveChanges();


            return RedirectToAction("UserPanel","Admin");
        }
    }
}
