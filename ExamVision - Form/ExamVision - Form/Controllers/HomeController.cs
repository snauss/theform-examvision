﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExamVision___Form.Models;
using ExamVision___Form.ModelsView;
using Microsoft.AspNet.Identity;

namespace ExamVision___Form.Controllers
{
    public class HomeController : Controller
    {

        [HttpGet]
        [Authorize]
        public ActionResult Index()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            var userId = User.Identity.GetUserId();
            var user = db.Users.SingleOrDefault(x => x.Id == userId);
            ViewBag.userMail = user.Email;
           

            ViewBag.Cosmo = db.Glasses.SingleOrDefault(x => x.Name == "Cosmo");
            ViewBag.Sport = db.Glasses.SingleOrDefault(x => x.Name == "Sport");
            ViewBag.Icon = db.Glasses.SingleOrDefault(x => x.Name == "Icon");
            ViewBag.Essential = db.Glasses.SingleOrDefault(x => x.Name == "Essential");

            FormViewModel model = new FormViewModel();
            model.Form = new FormModel();

            return View(model);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Index(FormViewModel formModel)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            if (ModelState.IsValid)
            {
                string uid = User.Identity.GetUserId();
                formModel.Form.User = db.Users.SingleOrDefault(x => x.Id == uid);
                formModel.Form.isOrdered = false;
                formModel.Form.ImportDate = DateTime.Now;


                // Save to DB
                db.FormModels.Add(formModel.Form);
                db.SaveChanges();

                // uploaded file
                if (formModel.UploadedFile != null && formModel.UploadedFile.ContentLength > 0)
                {
                    string directory = Server.MapPath("~/Content/ClientPics/");
                    string filename = "ClientImg_" + formModel.Form.ID +
                                      Path.GetExtension(formModel.UploadedFile.FileName);
                    var path = Path.Combine(directory, filename);

                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);

                    formModel.UploadedFile.SaveAs(path);
                    formModel.Form.picture_upload_data_field = "/Content/ClientPics/" + filename;

                    // Save again to save the image properties.
                    db.FormModels.AddOrUpdate(formModel.Form);
                    db.SaveChanges();
                }
                
                return RedirectToAction("EditConfirmation", "Form", new { id = formModel.Form.ID });
            }

            ViewBag.Cosmo = db.Glasses.SingleOrDefault(x => x.Name == "Cosmo");
            ViewBag.Sport = db.Glasses.SingleOrDefault(x => x.Name == "Sport");
            ViewBag.Icon = db.Glasses.SingleOrDefault(x => x.Name == "Icon");
            ViewBag.Essential = db.Glasses.SingleOrDefault(x => x.Name == "Essential");

            var userId = User.Identity.GetUserId();
            var user = db.Users.SingleOrDefault(x => x.Id == userId);
            ViewBag.userMail = user.Email;

            return View(formModel);
        }
    }
}