﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExamVision___Form.Models
{
    public class FormModel
    {
        //Default value = True

        public Int32 ID { get; set; }

        [MaxLength(245)]
        public string comments { get; set; }

        [Required]
        [MaxLength(30)]
        [Display(Name = "F_32")]
        public string dealer_name { get; set; }

        [Required]
        [MaxLength(80)]
        public string dealer_mail { get; set; }

        [MaxLength(50)]
        public string dealer_reference_number { get; set; }

        [Required]
        [MaxLength(50)]
        public string client_name { get; set; }

        [MaxLength(50)]
        public string client_address { get; set; }

        [MaxLength(50)]
        public string client_address2 { get; set; }

        [MaxLength(30)]
        public string client_phonefax { get; set; }


        public string client_birth { get; set; }

        [MaxLength(50)]
        public string client_occupation { get; set; }

        [MaxLength(80)]
        public string client_use_optics_kind { get; set; }

        [MaxLength(100)]
        public string client_wear_glasses { get; set; }

        [MaxLength(10)]
        public string right_sph { get; set; }

        [MaxLength(10)]
        public string right_cyl { get; set; }

        [MaxLength(10)]
        public string right_axis { get; set; }

        [MaxLength(10)]
        public string right_add { get; set; }

        [MaxLength(10)]
        public string right_prism { get; set; }

        [MaxLength(10)]
        public string right_base { get; set; }

        [MaxLength(10)]
        public string right_va { get; set; }

        [MaxLength(10)]
        public string left_sph { get; set; }

        [MaxLength(10)]
        public string left_cyl { get; set; }

        [MaxLength(10)]
        public string left_axis { get; set; }

        [MaxLength(10)]
        public string left_add { get; set; }

        [MaxLength(10)]
        public string left_prism { get; set; }

        [MaxLength(10)]
        public string left_base { get; set; }

        [MaxLength(10)]
        public string left_va { get; set; }

        [MaxLength(10)]
        public string present_working_range { get; set; }

        [MaxLength(10)]
        public string preferred_working_range { get; set; }

        [MaxLength(20)]
        public string loupe_magnification_type { get; set; }

        [MaxLength(20)]
        public string loupe_frame_type { get; set; }

        [MaxLength(20)]
        public string loupe_frame_size { get; set; }

        [MaxLength(20)]
        public string loupe_frame_colour { get; set; }

        [MaxLength(20)]
        public string loupe_angle { get; set; }

        [MaxLength(20)]
        public string lens_options { get; set; }

        [MaxLength(20)]
        public string loupe_light_system { get; set; }

        [MaxLength(20)]
        public string loupe_light_other_system { get; set; }

        [MaxLength(20)]
        public string ipd_inf_right { get; set; }

        [MaxLength(20)]
        public string ipd_inf_total { get; set; }

        [MaxLength(20)]
        public string ipd_inf_left { get; set; }

        [MaxLength(20)]
        public string ipd_40cm_right { get; set; }

        [MaxLength(20)]
        public string ipd_40cm_total { get; set; }

        [MaxLength(20)]
        public string ipd_40cm_left { get; set; }

        [MaxLength(20)]
        public string ipd_30cm_right { get; set; }

        [MaxLength(20)]
        public string ipd_30cm_total { get; set; }

        [MaxLength(20)]
        public string ipd_30cm_left { get; set; }

        [MaxLength(20)]
        public string ipd_height_right { get; set; }

        [MaxLength(20)]
        public string ipd_height_left { get; set; }

        [MaxLength(20)]
        public string ipd_vertex_right { get; set; }

        [MaxLength(20)]
        public string ipd_vertex_left { get; set; }

        [MaxLength(20)]
        public string setup_demo_right { get; set; }

        [MaxLength(20)]
        public string setup_demo_left { get; set; }

        [MaxLength(20)]
        public string control_distances_from { get; set; }

        [MaxLength(20)]
        public string control_distances_to { get; set; }

        [MaxLength(20)]
        public string outer_lenses_type { get; set; }

        [MaxLength(20)]
        public string monofocal_distance { get; set; }

        [MaxLength(20)]
        public string bifocal_version { get; set; }

        [MaxLength(20)]
        public string bifocal_distant { get; set; }

        [MaxLength(20)]
        public string bifocal_near { get; set; }

        [MaxLength(30)]
        public string bifocal_type { get; set; }

        [MaxLength(30)]
        public string out_lens_prescrip_right_sph { get; set; }

        [MaxLength(30)]
        public string out_lens_prescrip_right_cyl { get; set; }

        [MaxLength(30)]
        public string out_lens_prescrip_right_axis { get; set; }

        [MaxLength(30)]
        public string out_lens_prescrip_right_add { get; set; }

        [MaxLength(30)]
        public string out_lens_prescrip_right_prism { get; set; }

        [MaxLength(30)]
        public string out_lens_prescrip_right_base { get; set; }

        [MaxLength(30)]
        public string out_lens_prescrip_right_va { get; set; }

        [MaxLength(30)]
        public string out_len_prescrip_left_sph { get; set; }

        [MaxLength(30)]
        public string out_lens_prescrip_left_cyl { get; set; }

        [MaxLength(30)]
        public string out_lens_prescrip_left_axis { get; set; }

        [MaxLength(30)]
        public string out_lens_prescrip_left_add { get; set; }

        [MaxLength(30)]
        public string out_lens_prescrip_left_prism { get; set; }

        [MaxLength(30)]
        public string out_lens_prescrip_left_base { get; set; }

        [MaxLength(30)]
        public string out_lens_prescrip_left_va { get; set; }

        [MaxLength(30)]
        public string frame_temple_r { get; set; }

        [MaxLength(30)]
        public string frame_temple_l { get; set; }

        [MaxLength(30)]
        public string frame_width { get; set; }

        [MaxLength(30)]
        public string frame_nose_ridge { get; set; }

        [MaxLength(30)]
        public string frame_engraved_name { get; set; }

        [MaxLength(30)]
        public string client_use_optics { get; set; }

        [MaxLength(30)]
        public string monofocal_type { get; set; }

        [MaxLength(30)]
        public string nav_id { get; set; }

        [MaxLength(30)]
        public string item_no_plano { get; set; }

        [MaxLength(30)]
        public string item_no_prescription { get; set; }

        [MaxLength(30)]
        public string item_no_frame { get; set; }

        [MaxLength(30)]
        public string item_no_light { get; set; }

        [DefaultValue(0)]
        public Int32 flag { get; set; }

        [MaxLength(30)]
        public string loupe_magnification { get; set; }

        [MaxLength(30)]
        public string loupe_acce_vshield_small { get; set; }

        [MaxLength(30)]
        public string loupe_attachment { get; set; }

        [MaxLength(30)]
        public string loupe_acce_vshield_large { get; set; }

        [MaxLength(30)]
        public string loupe_acce_curing_filter { get; set; }

        [MaxLength(30)]
        public string progressive_version { get; set; }

        [MaxLength(30)]
        public string dealer_alternative_email { get; set; }

        [MaxLength(30)]
        public string lens_option_prisma { get; set; }

        [MaxLength(30)]
        public string lens_option_shv { get; set; }

        [MaxLength(30)]
        public string fake { get; set; }

        [MaxLength(30)]
        public string order_button { get; set; }

        [MaxLength(30)]
        public string cosmo_deep_version { get; set; }

        [MaxLength(30)]
        public string salespersons_name { get; set; }

        [MaxLength(30)]
        public string loupe_angle_other { get; set; }

        [MaxLength(30)]
        public string sideshields { get; set; }

        [MaxLength(30)]
        public string nose_pads { get; set; }

        [MaxLength(30)]
        public string generate { get; set; }

        [MaxLength(30)]
        public string suitcase { get; set; }

        [MaxLength(30)]
        public string lens_option_aircoating { get; set; }

        [MaxLength(30)]
        public string light_system { get; set; }

        [MaxLength(30)]
        public string loupe_light_control_unit { get; set; }

        [MaxLength(30)]
        public string loupe_light_accesories_curing { get; set; }

        [MaxLength(30)]
        public string loupe_light_accesories_handsfree { get; set; }

        [MaxLength(30)]
        public string loupe_accesories_sideshield { get; set; }

        [MaxLength(30)]
        public string loupe_light_accesories_clip { get; set; }

        [MaxLength(15)]
        public string control_distances_from_left { get; set; }

        [MaxLength(15)]
        public string control_distances_to_left { get; set; }

        [MaxLength(15)]
        public string control_distances_from_right { get; set; }

        [MaxLength(15)]
        public string control_distances_to_right { get; set; }

        [MaxLength(15)]
        public string loupe_light_accesories_headband { get; set; }

        [MaxLength(15)]
        public string loupe_light_accesories_connect { get; set; }

        [MaxLength(15)]
        public string lens_option { get; set; }

        [MaxLength(15)]
        public string diamond_left { get; set; }

        [MaxLength(15)]
        public string diamond_right { get; set; }

        public bool rebuildToBool
        {
            get { return rebuild == 1; }
            set { rebuild = value ? 1 : 0; }
        }
        [DefaultValue(0)]
        public Int32 rebuild { get; set; }

        public bool reuse_frameToBool
        {
            get { return reuse_frame == 1; }
            set { reuse_frame = value ? 1 : 0; }
        }
        [DefaultValue(0)]
        public Int32 reuse_frame { get; set; }

        public bool reuse_orcularToBool
        {
            get { return reuse_orcular == 1; }
            set { reuse_orcular = value ? 1 : 0; }
        }
        [DefaultValue(0)]
        public Int32 reuse_orcular { get; set; }

        public bool light_order_onlyToBool
        {
            get { return light_order_only == 1; }
            set { light_order_only = value ? 1 : 0; }
        }
        [DefaultValue(0)]
        public Int32 light_order_only { get; set; }

        [MaxLength(120)]
        public string rebuild_notes { get; set; }

        [MaxLength(45)]
        public string discount_code { get; set; }

        [MaxLength(120)]
        public string other_glasses_description { get; set; }

        [MaxLength(120)]
        public string monovision_description { get; set; }

        [MaxLength(120)]
        public string notes_for_the_presription { get; set; }

        [MaxLength(45)]
        public string pd_inf_other_distance { get; set; }

        [MaxLength(45)]
        public string pd_inf_other_dist_right { get; set; }

        [MaxLength(45)]
        public string pd_inf_other_dist_left { get; set; }

        [MaxLength(45)]
        public string pd_inf_other_dist_total { get; set; }

        [MaxLength(45)]
        public string nosepad_color { get; set; }

        [MaxLength(45)]
        public string picture_upload_data_field { get; set; }

        [MaxLength(120)]
        public string pd_notes { get; set; }

        [MaxLength(45)]
        public string streetname { get; set; }

        [MaxLength(45)]
        public string city_zip { get; set; }

        [MaxLength(45)]
        public string country_State_Region { get; set; }

        [MaxLength(45)]
        public string latest_eye_test { get; set; }

        [MaxLength(120)]
        public string carrierLens_notes { get; set; }

        [MaxLength(120)]
        public string frameFitting_notes { get; set; }

        [MaxLength(45)]
        public string test_orderscol { get; set; }


        public bool isOrdered { get; set; }

        [MaxLength(25)]
        public string usersLastLogin { get; set; }



        public ApplicationUser User { get; set; }

        public string loupe_light_accessories_charger { get; set; }
        //public string loupe_light_accesories_charger { get; set; }
        public string lens_option_arcoating { get; set; }
        public DateTime ImportDate { get; set; }


        //System-based fields
        public string ClientCity { get; set; }
        public string ClientZip { get; set; }
        public string ClientRegion { get; set; }
        public string ClientCountry { get; set; }

        public string ClientDoWearGlasses { get; set; }
        public string ClientOpticForm { get; set; }
        public string ClientContactType { get; set; }
        public string ClientGlassType { get; set; }


        public string ClientCarrierLensVersion { get; set; }


        //public bool SOCreated { get; set; }
        //public bool SLCreated { get; set; }
        //public bool DealerCreated { get; set; }
        //public bool ClientCreated { get; set; }
        //public bool Transf { get; set; }
        //public bool Prescription { get; set; }
        //public string SOID { get; set; }
        //public int WEBID { get; set; }
        //public bool Interrupted { get; set; }





    }
}