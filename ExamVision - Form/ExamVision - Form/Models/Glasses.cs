﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamVision___Form.Models
{
    public class Glasses
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Small { get; set; }
        public bool Medium { get; set; }
        public bool Large { get; set; }
        public bool Grey { get; set; }
        public bool Red { get; set; }
        public bool Black { get; set; }
        public bool Raw { get; set; }
        public bool Silver { get; set; }
        public bool DiamondLeft { get; set; }
        public bool DiamondRight { get; set; }

    }
}