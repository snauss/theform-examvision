﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExamVision___Form.Models;

namespace ExamVision___Form.ModelsView
{
    public class AdminPanelViewModel
    {
        public Glasses Cosmo { get; set;} 
        public Glasses Sport { get; set;} 
        public Glasses Icon { get; set;} 
        public Glasses Essential { get; set;} 
        public List<FormModel> Completed { get; set; }
    }
}