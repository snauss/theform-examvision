﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ExamVision___Form.Models;

namespace ExamVision___Form.ModelsView
{
    public class FormViewModel
    {
        //public Glasses Cosmo { get; set; }
        //public Glasses Sport { get; set; }
        //public Glasses Icon { get; set; }
        //public Glasses Essential { get; set; }

        public FormModel Form { get; set; }

        [Display(Name = "Image")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase UploadedFile { get; set; }

    }
}