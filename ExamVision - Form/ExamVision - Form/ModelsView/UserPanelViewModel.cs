﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExamVision___Form.Models;

namespace ExamVision___Form.ModelsView
{
    public class UserPanelViewModel
    {
        public List<FormModel> Incomplete { get; set;}
        public List<FormModel> Completed { get; set;}
    }
}