﻿$(document).ready(function () {

    //Convert 0/1 to No/Yes
    //-------------------------------------------
    //Request strings
    YesNoConverter("rebuildDetails", "rebuildDetailsResults");
    YesNoConverter("reuseFrameDetails", "reuseFrameDetailsResult");
    YesNoConverter("reuseOrcularDetails", "reuseOrcularDetailsResult");
    YesNoConverter("lightOrderDetails", "lightOrderDetailsResult");


    //Converter function
    function YesNoConverter(details, result) {
        var theDetails = document.getElementById(details).innerHTML.trim();
        var theResults = document.getElementById(result);
        if (theDetails === "0") {
            theResults.innerHTML = theDetails.trim().replace("0", "No");
        }
        if (theDetails === "1") {
            theResults.innerHTML = theDetails.trim().replace("1", "Yes")
        }
    };


    function UnitAdder(details, detailResult, unit) {
        var details = document.getElementById(details).innerHTML;
        var detailResult = document.getElementById(detailResult);
        if (details != "") {
            detailResult.innerHTML = details + " " + unit;
        }
    }


    //Order details frame corrector
    //--------------------------------------------
    //Type
    var frametype = document.getElementById("oDetailFrameType").innerHTML;
    var frametypeResult = document.getElementById("oDetailFrameTypeResult");
    if (frametype != "") {
        frametypeResult.innerHTML = frametype.trim().replace(" Frame", "");
        frametypeResult = frametype.trim().replace(" Frame", "");

    }

    //Color
    var framecolor = document.getElementById("oDetailFrameColor").innerHTML;
    var framecolorResult = document.getElementById("oDetailFrameColorResult");
    if (framecolor != "") {
        framecolorResult.innerHTML = framecolor.replace(frametypeResult.toLowerCase(), "").replace("Cosmo", "");
    }

    //Size
    var frameSize = document.getElementById("oDetailFrameSize").innerHTML;
    var frameSizeResult = document.getElementById("oDetailFrameSizeResult");
    if (frameSize != "")
        frameSizeResult.innerHTML = frameSize.replace(frametypeResult.toLowerCase(), "").replace("Cosmo", "");
    //--------------------------------------------


    //Control Unit Detail replacer
    var controlUnit = document.getElementById("controlUnitDetails").innerHTML;
    var controlUnitResult = document.getElementById("controlUnitDetailsResult");
    if (controlUnit != "") {
        controlUnitResult.innerHTML = controlUnit.replace("Premium (Mkll)", "Focus Xtend Control Unit").replace("Essential", "Essential Control Unit");
    }

    //UnitAdder
    UnitAdder("presentWDDetail", "presentWDDetailResult", "cm");
    UnitAdder("PreferredWDDetail", "PreferredWDDetailResult", "cm");

    UnitAdder("pd_inf_other_distance", "pd_inf_other_distanceResult", "cm");
    UnitAdder("pd_inf_other_dist_right", "pd_inf_other_dist_rightResult", "mm");
    UnitAdder("pd_inf_other_dist_total", "pd_inf_other_dist_totalResult", "mm");
    UnitAdder("pd_inf_other_dist_left", "pd_inf_other_dist_leftResult", "mm");

    UnitAdder("frame_temple_l", "frame_temple_lResult", "mm");
    UnitAdder("frame_temple_r", "frame_temple_rResult", "mm");

    function UnitAdder(details, detailResult, unit) {
        var details = document.getElementById(details).innerHTML;
        var detailResult = document.getElementById(detailResult);
        if (details != "") {
            detailResult.innerHTML = details + " " + unit;
        }
    }




    //Loupe Accesories Conditional showing
    LoupeAcceCon("connectDetail", "#connectLine")
    LoupeAcceCon("shieldDetail", "#shieldLine")
    LoupeAcceCon("visionShieldSmallDetail", "#visionShieldSmallLine")
    LoupeAcceCon("visionShieldLargeDetail", "#visionShieldLargeLine")

    function LoupeAcceCon(detail, line) {
        var theDetail = document.getElementById(detail).innerHTML;
        if (theDetail == "") {
            $(line).addClass("hidden");
        }
    }



    //Chrome cheker
    //var isChrome = !!window.chrome && !!window.chrome.webstore;
    //if (isChrome) {

    //}
});