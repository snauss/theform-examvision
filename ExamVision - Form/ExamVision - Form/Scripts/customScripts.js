/**
 * Created by Strauss on 18-10-2016.
 */

$(document).ready(function () {
    //Username to data
    var unTemp = $("#staticUsername").text();
    $("#dealerFName").val(unTemp.trim());
    console.log($("#dealerFName").val());

    //Mail to data

    var mailTemp = $("#staticEmail").text();
    $("#email").val(mailTemp.trim());
    console.log($("#email").val());
});



//Rebuild checkbox
$(document).ready(function () {
    $("#rebuild").click(function () {
        if ($("#rebuildCheck").is(":checked")) {
            $("#rebuildOptions").removeClass("hidden");
            $("#lightOrderOnly").addClass("hidden");
            $("#lightOnlyInput").attr("checked", false);
            $("#lightOrderOnly").click();
        } else {
            $("#rebuildOptions").addClass("hidden");
            $("#lightOrderOnly").removeClass("hidden");
            $("#reuseFrameInput").prop("checked", false);
            $("#reuseOcularInput").prop("checked", false);
        }
    });
});

//ReuseFrame blocker
$(document).ready(function () {
    $("#rebuildOptions, #rebuild").click(function () {
        if ($("#reuseFrameInput").is(":checked")) {
            $("#frameReused").removeClass("hidden");
            $("#frameSelector").addClass("hidden");
            deselectorRadio("frameSelector");
        } else {
            $("#frameReused").addClass("hidden");
            $("#frameSelector").removeClass("hidden");
        }
    });
});

//ReuseOcular blocker
$(document).ready(function () {
    $("#rebuildOptions, #rebuild").click(function () {
        if ($("#reuseOcularInput").is(":checked")) {
            $("#ocularReused").removeClass("hidden");
            $("#magnificationPart").addClass("hidden");
            deselectorRadio("magnificationPart");
        } else {
            $("#ocularReused").addClass("hidden");
            $("#magnificationPart").removeClass("hidden");
        }
    });
});


//Light order only
$(document).ready(function () {
    $("#lightOrderOnly").click(function () {
        if ($("#lightOnlyInput").is(":checked")) {
            $("#prescriptionTable").addClass("hidden");
            $("#PDInformationSession").addClass("hidden");
            $("#carrierLens").addClass("hidden");
            $("#frameChoice").addClass("hidden");
            $("#fitting").addClass("hidden");
            $("#magnificationChoice").addClass("hidden");
            deselectorRadio("prescriptionTable");
            deselectorRadio("PDInformationSession");
            $(".infT, .40cmT, .30cmT, .prefT").html("");
            deselectorRadio("carrierLens");
            deselectorRadio("frameChoice");
            deselectorRadio("fitting");
            deselectorRadio("magnificationChoice");
        } else {
            $("#prescriptionTable").removeClass("hidden");
            $("#PDInformationSession").removeClass("hidden");
            $("#carrierLens").removeClass("hidden");
            $("#frameChoice").removeClass("hidden");
            $("#fitting").removeClass("hidden");
            $("#magnificationChoice").removeClass("hidden");
        }
    });
});

//clientCountry saver
$(document).ready(function () {
    var clientCountry = $("#clientCountry").val();
    if (clientCountry !== "") {
        $("#country").val(clientCountry);
    }
    $("#country").change(function () {
        $("#clientCountry").val($("#country").val());
    });

});


////Address1 combiner
$(document).ready(function () {
    $("#address").change(function () {
        var streetAddress = $("#streetAddress").val();
        var totalAddress = streetAddress;
        $("#clientAddress").val(totalAddress);

    });
});
////Address2 combiner
$(document).ready(function () {
    $("#address").change(function () {
        var city = $("#city").val();
        var zipCode = $("#zip").val();
        var region = $("#region").val();
        var country = $("#country").val();

        var totalAddress = city + " | " +
            zipCode + " | " +
            region + " | " +
            country + " | ";

        $("#clientAddress2").val(totalAddress);

    });
});

//city_zip combiner
$(document).ready(function () {
    $("#city, #zip").change(function () {
        var city = $("#city").val();
        var zipCode = $("#zip").val();

        var cityZip = zipCode + " " + city;

        $("#cityZip").val(cityZip);

    });
});


//state_country combiner
$(document).ready(function () {
    $("#region, #country").change(function () {
        var region = $("#region").val();
        var country = $("#country").val();

        var regionCountry = region + " " + country;

        $("#regionCountry").val(regionCountry);

    });
});



//Age Checker
$(document).ready(function () {
    $("#yob").change(over40Checker());
});


var over40Checker = function () {
    var today = new Date();
    var yearOfBirth = parseInt($("#yob").val());
    var age = today.getFullYear() - yearOfBirth;
    if (age >= 40) {
        //console.log(age + " over 40");
        return 1;
    } else {
        //console.log(age + " under 40");
        return false;
    }
};

$(document).ready(function () {
    $("#yob").change(function () {
        var number = document.getElementById("yob").value;

        console.log(number + " = input tal!!");
        console.log("input is: " + number.toString().length + " long");

        if (number.toString().length <= 3) {
            $('#yob').addClass("presWarning");
            $('#shortAge').removeClass("hidden");
            console.log("short");
        } else if (number > 2010 || number < 1930) {
            $('#yob').addClass("presWarning");
            $('#wrongAge').removeClass("hidden");
            $('#shortAge').addClass("hidden");
            console.log("wrong");
        }

        else if ((number < 2010 || number > 1930) && (number.toString().length === 4)) {
            $('#yob').removeClass("presWarning");
            $('#shortAge').addClass("hidden");
            $('#wrongAge').addClass("hidden");
            console.log("none11");
        }
    });
});

//Over 40 functions
$(document).ready(function () {
    $("#yob").change(function () {
        over40Checker();
        if (over40Checker()) {
            $('.over40Filter').removeClass('hidden');
            $('#monofocalFDSession').removeClass("hidden");
            $('#monoFocus').removeClass("monoFocalOpt");
            $('#monoFocus').attr("checked", true);
            $('#monoFocus').required = true;
        } else if (!over40Checker()) {
            $('.over40Filter').addClass('hidden');
            $('#monoFocus').addClass("monoFocalOpt");
            $('#monoFocus').attr("checked", false);
            $('#monoFocus').required = false;
        }

    });
});


//Does the client use magnification optics? Yes -> Textarea
$(document).ready(function () {
    $("#clientMagChoice").click(function () {
        if ($('#magOptYes').is(':checked')) {
            $('#magnDesc').removeClass('hidden');
        } else {
            $('#magnDesc').addClass('hidden');
        }
    });
});

//Does the client wear glasses? - Yes -> Info about type
$(document).ready(function () {
    $("#clientWearsGlass").click(function () {
        if ($('#cGlassYes').is(':checked')) {
            $('#glassForm').removeClass('hidden');
            $('.over40Warning').addClass('hidden');
            $('#prescriptionInfo').removeClass('hidden');
            $('#noPrescriptionInfo').addClass('hidden');
        }
        if ($('#cGlassNo').is(':checked')) {
            $('#glassForm').addClass('hidden');
            $('.over40Warning').removeClass('hidden');
            $('#prescriptionInfo').addClass('hidden');
            $('#noPrescriptionInfo').removeClass('hidden');
        }
    });
});

////Client wears glasses combiner
//$(document).ready(function () {
//    $("#clientWearsGlass, #glassForm, #glassTypes, #otherGlass, #contactTypes, #monovisionDescField ").change(function () {

//        var wearsGlass = $('input[name="Form.ClientDoWearGlasses"]:checked').val();
//        var opticForm = $('input[name="Form.ClientOpticForm"]:checked').val();
//        var contactType = $("#contactType").val();
//        var glassType = $("#glassType").val();

//        if (opticForm === "Glasses") {
//            contactType = "";
//        }

//        if (opticForm === "Contacts") {
//            glassType = "";
//        }

//        var clientGlasses = wearsGlass + " " +
//                           opticForm + " " +
//                           contactType + " " +
//                           glassType + " ";

//        $("#clientWearsGlasses").val(clientGlasses);

//    });
//});

//Client wears glasses combiner
$(document).ready(function () {
    $("#clientWearsGlass, #glassForm, #glassTypes, #otherGlass, #contactTypes, #monovisionDescField ").change(function () {

        var wearsGlass = $('input[name="Form.ClientDoWearGlasses"]:checked').val();
        var opticForm = $('input[name="Form.ClientOpticForm"]:checked').val();
        var contactType = $("#contactType").val();
        var glassType = $("#glassType").val();

        if (opticForm === "Glasses") {
            $("#contactType").val("");
            contactType = "";
        }

        if (opticForm === "Contacts") {
            $("#glassType").val("");
            glassType = "";
        }

        var clientGlasses = contactType + glassType;

        $("#clientWearsGlasses").val(clientGlasses);

    });
});



$(document).ready(function () {
    $("#furtherClientInfoRow").click(function () {
        if ($('#magOptYes,#cGlassYes').is(':checked')) {
            $('#furtherClientInfoRow').addClass('bgColor');
        }
        else {
            $('#furtherClientInfoRow').removeClass('bgColor');
        }
    })
});

//Types of glasses
$(document).ready(function () {
    $("#glassForm").click(function () {
        if ($('#wearsGlasses').is(':checked')) {
            $('#glassTypes').removeClass('hidden');
            $('#otherGlass').removeClass('hidden');
            $('#contactTypes').addClass('hidden');
        }
        if ($('#wearsContacts').is(':checked')) {
            $('#glassTypes').addClass('hidden');
            $('#otherGlass').addClass('hidden');
            $('#contactTypes').removeClass('hidden');
        }
    });
});


//Contacts notefield
$(document).ready(function () {
    $("#contactTypes").change(function () {
        var contactType = $("#contactType").val();
        if (contactType === "Monovision") {
            $('#monovisionDescField').removeClass('hidden');
        } else {
            $('#monovisionDescField').addClass('hidden');
        }
    });
});


//shp-trigger
$(document).ready(function () {
    $("#glassType, #contactType, #glassForm ").change(function () {
        if ($("#wearsGlasses").is(':checked')) {
            if ($("#glassType").val() === "Glasses distance, near-monofocal" || $("#glassType").val() === "Near Only") {
                $(".shp").addClass("presWarning");
                $(".shpWarrning").removeClass("hidden");
                $(".add40cm").removeClass("presWarning");
                $(".add40cmWarrning").addClass("hidden");
                $("#shpController").removeClass("hidden");
                console.log($("#glassType").val() + " testeren1");
            } else if ($("#glassType").val() === "Bifocal" || $("#glassType").val() === "Multifocal") {
                $(".shp").addClass("presWarning");
                $(".add40cm").addClass("presWarning");
                $(".shpWarrning").removeClass("hidden");
                $(".add40cmWarrning").removeClass("hidden");
                $("#shpController").removeClass("hidden");
                console.log($("#glassType").val() + " testeren2");
            }
        } else {
            $(".shp").removeClass("presWarning");
            $(".add40cm").removeClass("presWarning");
            $(".shpWarrning").addClass("hidden");
            $(".add40cmWarrning").addClass("hidden");
            $("#shpController").addClass("hidden");
            console.log($("#glassType").val() + " testeren3");
        }
    });
});


$("#dateEyeTest").datepicker({
    format: "mm/yyyy",
    minViewMode: 1,
    todayHighlight: true
});



//Rounding to nearst 0.25
function roundToQuarter(input) {
    var x = input.value;
    x = (Math.round(x * 4) / 4).toFixed(2);
    input.value = x;
}

//shp limit 
function shpLimit(input) {
    var x = input.value;
    if (x > 10 || x < -10 || x === 10 || x === -10) {
        $("#sphLimitWarning").fadeIn();

        $("#sphLimitWarning").removeClass("hidden").delay(4500).queue(function () {
            $(this).addClass("hidden").dequeue();
        });
        if (x > 10) {
            input.value = 10;
        }
        if (x < -10) {
            input.value = -10;
        }
    }
}

//add(40cm) limit
function addLimit(input) {
    var x = input.value;
    if (x > 4 || x < -4 || x === 4 || x === -4) {
        $("#addLimitWarning").fadeIn();

        $("#addLimitWarning").removeClass("hidden").delay(4500).queue(function () {
            $(this).addClass("hidden").dequeue();
        });
        if (x > 4) {
            input.value = 4;
        }
        if (x < -4) {
            input.value = -4;
        }
    }
}



//No cyl without axis + No prism without base
$(document).ready(function () {
    $("#cylR, #axisR, #cylL, #axisL").change(function () {
        var cylInput = $("#cylR").val() + $("#cylL").val();
        var axisInput = $("#axisR").val() + $("#axisL").val();

        if (cylInput.length !== 0 && axisInput.length === 0) {
            $(".cylAxis").addClass("presWarning");
            $("#onlyCyl").removeClass("hidden");
            $("#cylAxisController").removeClass("hidden");
            console.log("test1");
        }
        else if (axisInput.length !== 0 && cylInput.length === 0) {
            $(".cylAxis").addClass("presWarning");
            $("#onlyAxis").removeClass("hidden");
            $("#cylAxisController").removeClass("hidden");
            console.log("test1");
        } else if (axisInput.length !== 0 && cylInput.length !== 0) {
            $(".cylAxis").removeClass("presWarning");
            $("#onlyCyl").addClass("hidden");
            $("#onlyAxis").addClass("hidden");
            $("#cylAxisController").addClass("hidden");
            console.log("test1");
        }
    });
    $("#prismR, #baseR, #prismL, #baseL").change(function () {
        var cylInput = $("#prismR").val() + $("#prismL").val();
        var axisInput = $("#baseR").val() + $("#baseL").val();

        if (cylInput.length !== 0 && axisInput.length === 0) {
            $(".prismBase").addClass("presWarning");
            $("#onlyPrism").removeClass("hidden");
            $("#prismBaseController").removeClass("hidden");
        }
        else if (axisInput.length !== 0 && cylInput.length === 0) {
            $(".prismBase").addClass("presWarning");
            $("#onlyBase").removeClass("hidden");
            $("#prismBaseController").removeClass("hidden");
        } else {
            $(".prismBase").removeClass("presWarning");
            $("#onlyPrism").addClass("hidden");
            $("#onlyBase").addClass("hidden");
            $("#prismBaseController").addClass("hidden");
        }
    });
});

//SHV-version | No cyl without axis + No prism without base
$(document).ready(function () {
    $("#mfCylR, #mfAxisR, #mfCylL, #mfAxisL").change(function () {
        var cylInput = $("#mfCylR").val() + $("#mfCylL").val();
        var axisInput = $("#mfAxisR").val() + $("#mfAxisL").val();

        if (cylInput.length !== 0 && axisInput.length === 0) {
            $(".mfCylAxis").addClass("presWarning");
            $("#mfOnlyCyl").removeClass("hidden");
            $("#mfCylAxisController").removeClass("hidden");
        }
        else if (axisInput.length !== 0 && cylInput.length === 0) {
            $(".mfCylAxis").addClass("presWarning");
            $("#mfOnlyAxis").removeClass("hidden");
            $("#mfCylAxisController").removeClass("hidden");
        } else {
            $(".mfCylAxis").removeClass("presWarning");
            $("#mfOnlyCyl").addClass("hidden");
            $("#mfCylAxisController").addClass("hidden");
        }
    });
    $("#mfPrismR, #mfBaseR, #mfPrismL, #mfBaseL").change(function () {
        var cylInput = $("#mfPrismR").val() + $("#mfPrismL").val();
        var axisInput = $("#mfBaseR").val() + $("#mfBaseL").val();

        if (cylInput.length !== 0 && axisInput.length === 0) {
            $(".mfPrismBase").addClass("presWarning");
            $("#mfOnlyPrism").removeClass("hidden");
            $("#mfPrismBaseController").removeClass("hidden");
        }
        else if (axisInput.length !== 0 && cylInput.length === 0) {
            $(".mfPrismBase").addClass("presWarning");
            $("#mfOnlyBase").removeClass("hidden");
            $("#mfPrismBaseController").removeClass("hidden");
        } else {
            $(".mfPrismBase").removeClass("presWarning");
            $("#mfOnlyPrism").addClass("hidden");
            $("#mfOnlyBase").addClass("hidden");
            $("#mfPrismBaseController").addClass("hidden");
        }
    });
});


//Comma to dot
jQuery.fn.ForceNumericOnly =
    function () {
        this.keyup(function (e) {
            //console.log($(this).val($(this).val()));
            $(this).val($(this).val().replace(/,/g, "."));
        });
    };
$("#pdTable input").ForceNumericOnly();



////Maybe use
//$("input[type=number]").keyup(function (e) {
//    var key = e.which ? e.which : event.keyCode;
//    if (key == 110 || key == 188) {
//        e.preventDefault();
//        var value = $(this).val();
//        console.log(value === "");
//        $(this).val(value.replace(",", "."));
//    }
//});






//PD Information - Table calculator
$(document).ready(function () {
    $("#pdTable").change(function () {

        calculateSum(".inf", ".infT", "#infT");
        calculateSum(".40cm", ".40cmT", "#40cmT");
        calculateSum(".30cm", ".30cmT", "#30cmT");
        calculateSum(".pref", ".prefT", "#prefT");
        //console.log($("#30cmT").val());
    });
    $("#pdTable").trigger("change");
});


function calculateSum(ipd, totalHtml, totalId) {
    var sum = 0;
    //iterate through each textboxes and add the values
    $(ipd).each(function () {

        //add only if the value is number
        if (!isNaN(this.value) && this.value.length !== 0) {
            sum += parseFloat(this.value);
        }
    });
    //.toFixed() method will roundoff the final sum to 2 decimal places
    $(totalHtml).html(sum.toFixed(2));
    $(totalId).val(sum.toFixed(2));
}




//PD Information Min/Max
$(document).ready(function () {
    $("#PDInformationSession").change(function () {
        PDMinMax("#presentWD", "#PresentWDWarn", 15, 100);
        PDMinMax("#desiredWD", "#PreferredWDWarn", 15, 100);
        PDMinMax("#loupeAngle", "#LoupeAngleWarn", 0, 40);

        IPDTableMinMax("#infT", "#IPDWarn", 40, 99.99);
        IPDTableMinMax("#40cmT", "#IPDWarn", 40, 99.99);
        IPDTableMinMax("#30cmT", "#IPDWarn", 40, 99.99);
        IPDTableMinMax("#prefT", "#IPDWarn", 40, 99.99);
    });
});



function PDMinMax(input, output, min, max) {
    var theInput = $(input).val();
    if (theInput == 0) {
    }
    else if (theInput < min) {
        $(output).html("Value must be > " + min);
        $(output).fadeIn
        $(output).removeClass("hidden").delay(3500).queue(function () { $(this).addClass("hidden").dequeue(); });
        $(input).val(min);
    }
    else if (theInput > max) {
        $(output).html("Value must be < " + max);
        $(output).fadeIn
        $(output).removeClass("hidden").delay(3500).queue(function () { $(this).addClass("hidden").dequeue(); });
        $(input).val(max);
    }
}



function IPDTableMinMax(input, output, min, max) {
    var theInput = $(input).val();
    if (theInput == 0) {
    }
    else if (theInput < min || theInput > max) {
        $(output).html("Total value must be between " + min + " and " + max);
        $(output).fadeIn
        $(output).removeClass("hidden");
        $(input).parent().addClass("presWarning");
    }
    else if (theInput > min || theInput < max) {
        $(output).addClass("hidden");
        $(input).parent().removeClass("presWarning");
    }
}




////shp limit 
//function shpLimit(input) {
//    var x = input.value;
//    if (x > 10 || x < -10 || x === 10 || x === -10) {
//        $("#sphLimitWarning").fadeIn();

//        $("#sphLimitWarning").removeClass("hidden").delay(4500).queue(function () {
//            $(this).addClass("hidden").dequeue();
//        });
//        if (x > 10) {
//            input.value = 10;
//        }
//        if (x < -10) {
//            input.value = -10;
//        }
//    }
//}



//var frametype = document.getElementById("oDetailFrameType").innerHTML;
//var frametypeResult = document.getElementById("oDetailFrameTypeResult");
//if (frametype != "") {
//    frametypeResult.innerHTML = frametype.trim().replace(" Frame", "");
//    frametypeResult = frametype.trim().replace(" Frame", "");

//}



//Print Limitation Table
var printLimitationTable = function () {
    var w = window.open("../Content/Files/Limitation_Table.pdf");
    w.print();
};

$(document).ready(function () {
    $("#printLT").click(function () {
        printLimitationTable();
    });
});


var deselectorRadio = function (target) {
    $("#" + target + " input").prop("checked", false);
    //$("#" + target + " input").val("");
}

//Carrier Lens Selector
$(document).ready(function () {
    $("#carrierLensSelector").change(function () {
        if ($("#carrierLensSelector").val() === "Plano") {
            //console.log("carrier lens tester1");

            $("#carrierLensVersionPart").addClass("hidden");
            $("#coatingOpt").removeClass("hidden");
        } else if ($("#carrierLensSelector").val() === "Prescription") {
            //console.log("carrier lens tester2");

            $("#carrierLensVersionPart").removeClass("hidden");
            $("#coatingOpt").removeClass("hidden");
            $("#shvTable").removeClass("hidden");
        } else if ($("#carrierLensSelector").val() === "PlanoRX") {
            //console.log("carrier lens tester3");

            $("#carrierLensVersionPart").addClass("hidden");
            $("#coatingOpt").removeClass("hidden");
        } else {
            //console.log("carrier lens tester4");

            $("#carrierLensVersionPart").addClass("hidden");
            $("#focusDistance").addClass("hidden");
            $("#bifocalType").addClass("hidden");
            $("#progressiveVersion").addClass("hidden");
            $("#coatingOpt").addClass("hidden");
            $("#shvTable").addClass("hidden");
        }
    });
    $("#carrierLensSelector").trigger("change");
});

//var deselectorRadio = function (target) {
//    $("#" + target + " input").val("");
//    $("#" + target + " input").prop("checked", false);
//}

//Carrier Lens version Selector
$(document).ready(function () {
    $("#carrierLensVersion, #carrierLensSelector").change(function () {

        if ($("#carrierLensVersion").val() === "Monofocal" && $("#carrierLensSelector").val() === "Prescription") {
            $("#focusDistance").removeClass("hidden");
            $("#bifocalType").addClass("hidden");
            $("#progressiveVersion").addClass("hidden");
            deselectorRadio("bifocalType");
            deselectorRadio("progressiveVersion");
            console.log("focusDistance shown / monofocal");
        } else if ($("#carrierLensVersion").val() === "Bifocal" && $("#carrierLensSelector").val() === "Prescription") {
            $("#focusDistance").addClass("hidden");
            $("#bifocalType").removeClass("hidden");
            $("#progressiveVersion").addClass("hidden");
            deselectorRadio("progressiveVersion");
            deselectorRadio("monofocalFDSession");
            console.log("bifocalType shown / bifocal");
        } else if ($("#carrierLensVersion").val() === "Progressive Version" && $("#carrierLensSelector").val() === "Prescription") {
            $("#focusDistance").addClass("hidden");
            $("#bifocalType").addClass("hidden");
            $("#progressiveVersion").removeClass("hidden");
            deselectorRadio("monofocalFDSession");
            deselectorRadio("bifocalType");
            console.log("progressiveVersion shown / progressiveVersion");
        } else {
            $("#focusDistance").addClass("hidden");
            $("#bifocalType").addClass("hidden");
            $("#progressiveVersion").addClass("hidden");
        }
    });

    $("#carrierLensVersion, #carrierLensSelector").trigger("change");
});


$('label[data-toggle="tooltip"]').tooltip({
    animated: "",
    placement: "right",
    html: true
});



//Coating Radio
$(document).ready(function () {
    var accessory = $(".coatingOption");
    var selectedRadioBtn;
    var i = 0;
    for (i = 0; i < accessory.length; i++) {
        accessory[i].onclick = function () {
            if (selectedRadioBtn === this) {
                this.checked = false;
                selectedRadioBtn = null;
            } else {
                selectedRadioBtn = this;
            }
        };
    }
});

$(document).ready(function () {

    $("#arCoating").click(function () {
        $("#shvCoating").attr("checked", false);
    });
    $("#shvCoating").click(function () {
        $("#arCoating").attr("checked", false);
    });

});


//////Ar coating auto-selector
//$(document).ready(function () {
//    $("#coatingOpt").click(function () {
//        if ($("#arCoating").is(":checked")) {
//            $("#essentialFrame").attr("checked", true);
//            $("#arCoatNote").removeClass("hidden");
//        } else {
//            $("#arCoatNote").addClass("hidden");
//        }
//    });
//});

//$(document).ready(function () {
//    $("#essentialFrame").change(function () {
//        if (!$("#essentialFrame").is(":checked")) {
//            $("#arCoating").attr("checked", false);
//            $("#arCoatNote").addClass("hidden");
//        }
//    });
//});

$(document).ready(function () {
    $("#frameSelector").click(function () {

        if (!$("#essentialFrame").is(":checked")) {
            $("#arCoating").attr("checked", false);
            $("#arCoatNote").addClass("hidden");
        }
    });
});



$(document).ready(function () {
    $("#frameSelector, #coatingOpt").change(function () {

        if ($("#arCoating").is(":checked")) {
            $("#essentialFrame").prop("checked", true);
            $("#essentialFrame").trigger("click");
        }

        if ($("#essentialFrame").is(":checked")) {
            $("#essentialColorSilver").prop("checked", true);
        } else if (!$("#essentialFrame").is(":checked")) {
            deselectorRadio("essentialFrameColor");
        }
    });
});


//Second prescription table hidder
$(document).ready(function () {
    $("#ownLenses").change(function () {
        if ($('#ownLenses').is(':checked')) {
            $('#ownLensesTable').removeClass('hidden');
        } else {
            $('#ownLensesTable').addClass('hidden');
        }
    });
});






//Frame-selector
$(document).ready(function () {
    $("#frameSelector, #carrierLens").click(function () {
        if ($('#cosmoFrame').is(':checked')) {
            $('#cosmoFrameSize').removeClass('hidden');
            $('#cosmoFrameColor').removeClass('hidden');
        } else {
            $('#cosmoFrameSize').addClass('hidden');
            $('#cosmoFrameColor').addClass('hidden');
            deselectorRadio("cosmoFrameSize");
            deselectorRadio("cosmoFrameColor");
        }
        if ($('#sportFrame').is(':checked')) {
            $('#sportFrameSize').removeClass('hidden');
            $('#sportFrameColor').removeClass('hidden');
        } else {
            $('#sportFrameSize').addClass('hidden');
            $('#sportFrameColor').addClass('hidden');
            deselectorRadio("sportFrameSize");
            deselectorRadio("sportFrameColor");
        }
        if ($('#iconFrame').is(':checked')) {
            $('#iconFrameSize').removeClass('hidden');
            $('#iconFrameColor').removeClass('hidden');
            $('#iconDiamond').removeClass('hidden');
        } else {
            $('#iconFrameSize').addClass('hidden');
            $('#iconFrameColor').addClass('hidden');
            $('#iconDiamond').addClass('hidden');
            deselectorRadio("iconFrameSize");
            deselectorRadio("iconFrameColor");
            deselectorRadio("iconDiamond");
        }
        if ($('#essentialFrame').is(':checked')) {
            $('#essentialFrameSize').removeClass('hidden');
            $('#essentialFrameColor').removeClass('hidden');
        } else if (!$('#essentialFrame').is(':checked')) {
            $('#essentialFrameSize').addClass('hidden');
            $('#essentialFrameColor').addClass('hidden');
            //$("#arCoating").prop("checked", false);
            deselectorRadio("essentialFrameSize");
            deselectorRadio("essentialFrameColor");
        }
    });
});


//Diamond choice
$(document).ready(function () {
    var accessory = $(".diamondChoice");
    var selectedRadioBtn;
    var i = 0;
    for (i = 0; i < accessory.length; i++) {
        accessory[i].onclick = function () {
            if (selectedRadioBtn === this) {
                this.checked = false;
                selectedRadioBtn = null;
            } else {
                selectedRadioBtn = this;
            }
        };
    }
});


$(document).ready(function () {
    $("#magnificationSelector").change(function () {
        if ($('#keplerMag').is(':checked')) {
            $('#lensOptions').addClass('hidden');
        } else {
            $('#lensOptions').removeClass('hidden');
        }
    });
});


//Options - Prisma
$(document).ready(function () {
    var lensOptions = $(".lensOpt");
    var selectedRadioBtn;
    var i = 0;
    for (i = 0; i < lensOptions.length; i++) {
        lensOptions[i].onclick = function () {
            if (selectedRadioBtn === this) {
                this.checked = false;
                selectedRadioBtn = null;
            } else {
                selectedRadioBtn = this;
            }
        };
    }
});


//HD Galilean selector
$(document).ready(function () {
    $("#magnificationSelector").click(function () {
        if ($('#HDMag').is(':checked')) {
            $('#HDMagSub').removeClass('hidden');
        } else {
            $('#HDMagSub').addClass('hidden');
        }
        if ($('#essentialMag').is(':checked')) {
            $('#essentialMagSub').removeClass('hidden');
        } else {
            $('#essentialMagSub').addClass('hidden');
        }
        if ($('#keplerMag').is(':checked')) {
            $('#keplerMagSub').removeClass('hidden');
        } else {
            $('#keplerMagSub').addClass('hidden');
        }
    });
});

//Photo - uploader
$(document).ready(function () {
    $("#magnificationChoice").click(function () {
        if ($("#HDMag, #keplerMag").is(':checked')) {
            $('#photoUploader').removeClass('hidden');
        } else {
            $('#photoUploader').addClass('hidden');
        }
        if ($('#keplerMag').is(':checked')) {
            $('#photoWarning').removeClass('hidden');
        } else {
            $('#photoWarning').addClass('hidden');
        }
    });
});



//Show - Hide DemoTable
$(document).ready(function () {
    $("#magnificationChoice").click(function () {
        if ($('#demoTableCheck').is(':checked')) {
            $('#setupDemo').removeClass('hidden');
        } else {
            $('#setupDemo').addClass('hidden');
        }
    });
});


//Control units
$(document).ready(function () {
    var accessory = $(".ctrlUnits");
    var selectedRadioBtn;
    var i = 0;
    for (i = 0; i < accessory.length; i++) {
        accessory[i].onclick = function () {
            if (selectedRadioBtn === this) {
                this.checked = false;
                selectedRadioBtn = null;
            } else {
                selectedRadioBtn = this;
            }
        };
    }
});


//Control Units show/hide
$(document).ready(function () {
    $("#lightSystem").click(function () {
        if ($("#lightSystemNone").is(":checked")) {
            $("#controlUnitsFX,#controlUnitsE").attr("checked", false);
            $(".accessory").attr("checked", false);
            $("#controlUnits").addClass("hidden");
            $("#accessories").addClass("hidden");

        } else if ($("#lightSystemF4500,#lightSystemF6000,#lightSystemNano").is(":checked")) {
            $("#controlUnits").removeClass("hidden");
            $("#accessories").removeClass("hidden");
        }
    });
});


//Light system value filler
$(document).ready(function () {
    $(".lightSystemInput, .lightSystemInputNone").change(function () {
        if ($(".lightSystemInput").is(":checked")) {
            $("#lightSystemField").val("Mix/Match");
        }
        if ($(".lightSystemInputNone").is(":checked")) {
            $("#lightSystemField").val("None");
        }
    });
});


//Extra Nosepad, Color
$(document).ready(function () {
    var accessory = $(".extraNosepad");
    var selectedRadioBtn;
    var i = 0;
    for (i = 0; i < accessory.length; i++) {
        accessory[i].onclick = function () {
            if (selectedRadioBtn === this) {
                this.checked = false;
                selectedRadioBtn = null;
            } else {
                selectedRadioBtn = this;
            }
        };
    }
});


//Loupe-Accessories
$(document).ready(function () {
    var accessory = $(".loupeAccessory");
    var selectedRadioBtn;
    var i = 0;
    for (i = 0; i < accessory.length; i++) {
        accessory[i].onclick = function () {
            if (selectedRadioBtn === this) {
                this.checked = false;
                selectedRadioBtn = null;
            } else {
                selectedRadioBtn = this;
            }
        };
    }
});



//Accessories
$(document).ready(function () {
    var accessory = $(".accessory");
    var selectedRadioBtn;
    var i = 0;
    for (i = 0; i < accessory.length; i++) {
        accessory[i].onclick = function () {
            if (selectedRadioBtn === this) {
                this.checked = false;
                selectedRadioBtn = null;
            } else {
                selectedRadioBtn = this;
            }
        };
    }
});

//Radio acts as checkbox
$(document).ready(function () {
    $(":radio").each(function (i, obj) {
        if ($(obj).is(':checked')) {
            $(obj).click();
        }
    });
});

//Replaces "Enter" key with "Tab"
$('body').on('keydown', 'input, select, textarea', function (e) {
    var self = $(this)
        , form = self.parents('form:eq(0)')
        , focusable
        , next
        ;
    if (e.keyCode == 13) {
        focusable = form.find('input,a,select,button,textarea').filter(':visible');
        next = focusable.eq(focusable.index(this) + 1);
        if (next.length) {
            next.focus();
        } else {
            form.submit();
        }
        return false;
    }
});

//Limit numbers
function maxLengthCheck(object) {
    if (object.value.length > object.max.length)
        object.value = object.value.slice(0, object.max.length)
}

function isNumeric(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = false;
        if (theEvent.preventDefault) theEvent.preventDefault();
    }
}



// disable mousewheel on a input number field when in focus
// (to prevent Cromium browsers change the value when scrolling)
$('form').on('focus', 'input[type=number]', function (e) {
    $(this).on('mousewheel.disableScroll', function (e) {
        e.preventDefault();
    });
});
$('form').on('blur', 'input[type=number]', function (e) {
    $(this).off('mousewheel.disableScroll');
});

