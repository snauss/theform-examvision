﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ExamVision___Form.Startup))]
namespace ExamVision___Form
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
