﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExamVision___Form.Extensions
{
    public static class StringExtensions
    {
        public static Int32 Parse(this string str, Int32 defaultVal)
        {
            Int32 temp = defaultVal;
            Int32.TryParse(str, out temp);
            return temp;
        }
    }
}