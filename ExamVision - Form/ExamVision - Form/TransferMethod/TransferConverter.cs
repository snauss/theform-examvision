﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ExamVision___Form.Extensions;
using ExamVision___Form.Models;

namespace ExamVision___Form.TransferMethod
{
    public class TransferConverter
    {
        private static string TranslateColorValue(string color)
        {
            if (color == null)
                return null;

            switch (color.ToLower())
            {
                //Black
                case "iconblack":
                    return "Black";
                case "sportblack":
                    return "Black";

                //Red
                case "iconred":
                    return "Red";
                case "sportred":
                    return "Red";
                case "cosmored":
                    return "Red";

                //Raw
                case "iconraw":
                    return "Raw";

                //Grey
                case "sportgrey":
                    return "Grey";
                case "cosmogrey":
                    return "Grey";

                //Silver
                case "essentialsilver":
                    return "Silver";

                default:
                    return null; // in case we don't match anything
            }
        }

        private static string TranslateSizeValue(string size)
        {
            if (size == null)
                return null;

            switch (size.ToLower())
            {
                //Small
                case "iconsmall":
                    return "Small";
                case "sportsmall":
                    return "Small";
                case "cosmosmall":
                    return "Small";
                case "essentialsmall":
                    return "Small";

                //Medium
                case "iconmedium":
                    return "Medium";
                case "cosmomedium":
                    return "Medium";

                //Large
                case "iconlarge":
                    return "Large";
                case "sportlarge":
                    return "Large";
                case "cosmolarge":
                    return "Large";
                case "essentiallarge":
                    return "Large";


                default:
                    return null; // in case we don't match anything
            }
        }

        public static TransferModel Convert(FormModel model)
        {

            var result = new TransferModel
            {
                comments = model.comments ?? "",
                dealer_name = model.dealer_name,
                dealer_email = model.dealer_mail,
                dealer_reference_number = model.dealer_reference_number ?? "",
                client_name = model.client_name ?? "",
                client_address = model.client_address ?? "",
                client_address2 = model.client_address2 ?? "",
                client_phonefax = model.client_phonefax ?? "",
                //Check med Tvede om det er korrekt
                client_birth = model.client_birth.Parse(1960),
                client_occupation = model.client_occupation ?? "",
                client_use_optics_kind = model.client_use_optics_kind ?? "",
                client_wear_glasses = model.client_wear_glasses ?? "",
                right_sph = model.right_sph ?? "",
                right_cyl = model.right_cyl ?? "",
                right_axis = model.right_axis ?? "",
                right_add = model.right_add ?? "",
                right_prism = model.right_prism ?? "",
                right_base = model.right_base ?? "",
                right_va = model.right_va ?? "",
                left_sph = model.left_sph ?? "",
                left_cyl = model.left_cyl ?? "",
                left_axis = model.left_axis ?? "",
                left_add = model.left_add ?? "",
                left_prism = model.left_prism ?? "",
                left_base = model.left_base ?? "",
                left_va = model.left_va ?? "",
                present_working_range = model.present_working_range ?? "",
                preferred_working_range = model.preferred_working_range ?? "",
                loupe_magnification_type = model.loupe_magnification_type ?? "",
                loupe_frame_type = model.loupe_frame_type ?? "",
                loupe_frame_size = TranslateSizeValue(model.loupe_frame_size) ?? "",
                loupe_frame_colour = TranslateColorValue(model.loupe_frame_colour) ?? "",
                loupe_angle = model.loupe_angle ?? "",
                lens_options = model.lens_options ?? "",
                loupe_light_system = model.loupe_light_system ?? "",
                loupe_light_other_system = model.loupe_light_other_system ?? "",
                ipd_inf_right = model.ipd_inf_right ?? "",
                ipd_inf_total = model.ipd_inf_total ?? "",
                ipd_inf_left = model.ipd_inf_left ?? "",
                ipd_40cm_right = model.ipd_40cm_right ?? "",
                ipd_40cm_total = model.ipd_40cm_total ?? "",
                ipd_40cm_left = model.ipd_40cm_left ?? "",
                ipd_30cm_right = model.ipd_30cm_right ?? "",
                ipd_30cm_total = model.ipd_30cm_total ?? "",
                ipd_30cm_left = model.ipd_30cm_left ?? "",
                ipd_height_right = model.ipd_height_right ?? "",
                ipd_height_left = model.ipd_height_left ?? "",
                ipd_vertex_right = model.ipd_vertex_right ?? "",
                ipd_vertex_left = model.ipd_vertex_left ?? "",
                setup_demo_right = model.setup_demo_right ?? "",
                setup_demo_left = model.setup_demo_left ?? "",
                control_distances_from = model.control_distances_from ?? "",
                control_distances_to = model.control_distances_to ?? "",
                outer_lenses_type = model.outer_lenses_type ?? "",
                monofocal_distance = model.monofocal_distance ?? "",
                bifocal_version = model.bifocal_version ?? "",
                bifocal_distant = model.bifocal_distant ?? "",
                bifocal_near = model.bifocal_near ?? "",
                bifocal_type = model.bifocal_type ?? "",
                outer_lenses_prescription_right_sph = model.out_lens_prescrip_right_sph ?? "",
                outer_lenses_prescription_right_cyl = model.out_lens_prescrip_right_cyl ?? "",
                outer_lenses_prescription_right_axis = model.out_lens_prescrip_right_axis ?? "",
                outer_lenses_prescription_right_add = model.out_lens_prescrip_right_add ?? "",
                outer_lenses_prescription_right_prism = model.out_lens_prescrip_right_prism ?? "",
                outer_lenses_prescription_right_base = model.out_lens_prescrip_right_base ?? "",
                outer_lenses_prescription_right_va = model.out_lens_prescrip_right_va ?? "",
                outer_lenses_prescription_left_sph = model.out_len_prescrip_left_sph ?? "",
                outer_lenses_prescription_left_cyl = model.out_lens_prescrip_left_cyl ?? "",
                outer_lenses_prescription_left_axis = model.out_lens_prescrip_left_axis ?? "",
                outer_lenses_prescription_left_add = model.out_lens_prescrip_left_add ?? "",
                outer_lenses_prescription_left_prism = model.out_lens_prescrip_left_prism ?? "",
                outer_lenses_prescription_left_base = model.out_lens_prescrip_left_base ?? "",
                outer_lenses_prescription_left_va = model.out_lens_prescrip_left_va ?? "",
                frame_temple_r = model.frame_temple_r ?? "",
                frame_temple_l = model.frame_temple_l ?? "",
                frame_width = model.frame_width ?? "",
                frame_nose_ridge = model.frame_nose_ridge ?? "",
                frame_engraved_name = model.frame_engraved_name ?? "",
                client_use_optics = model.client_use_optics ?? "",
                monofocal_type = model.monofocal_type ?? "",
                nav_id = model.nav_id ?? "",
                item_no_plano = model.item_no_plano ?? "",
                item_no_prescription = model.item_no_prescription ?? "",
                item_no_frame = model.item_no_frame ?? "",
                item_no_light = model.item_no_light ?? "",
                flag = model.flag,
                loupe_magnification = model.loupe_magnification ?? "",
                loupe_accesories_vshield_small = model.loupe_acce_vshield_small ?? "",
                loupe_attachment = model.loupe_attachment ?? "",
                loupe_accesories_vshield_large = model.loupe_acce_vshield_large ?? "",
                loupe_accesories_curing_filter = model.loupe_acce_curing_filter ?? "",
                progressive_version = model.progressive_version ?? "",
                dealer_alternative_email = model.dealer_alternative_email ?? "",
                lens_option_prisma = model.lens_option_prisma ?? "",
                lens_option_shv = model.lens_option_shv ?? "",
                //Check med Tvede om det er korrekt
                fake = model.fake.Parse(0),
                order_button = model.order_button ?? "",
                cosmo_deep_version = model.cosmo_deep_version ?? "",
                salespersons_name = model.salespersons_name ?? "",
                loupe_angle_other = model.loupe_angle_other ?? "",
                sideshields = model.sideshields ?? "",
                nose_pads = model.nose_pads ?? "",
                generate = model.generate ?? "",
                suitcase = model.suitcase ?? "",
                lens_option_aircoating = model.lens_option_aircoating ?? "",
                light_system = model.light_system ?? "",
                loupe_light_control_unit = model.loupe_light_control_unit ?? "",
                loupe_light_accessories_curing = model.loupe_light_accesories_curing ?? "",
                loupe_light_accessories_handsfree = model.loupe_light_accesories_handsfree ?? "",
                loupe_accesories_sideshield = model.loupe_accesories_sideshield ?? "",
                loupe_light_accessories_clip = model.loupe_light_accesories_clip ?? "",
                control_distances_from_left = model.control_distances_from_left ?? "",
                control_distances_to_left = model.control_distances_to_left ?? "",
                control_distances_from_right = model.control_distances_from_right ?? "",
                control_distances_to_right = model.control_distances_to_right ?? "",
                loupe_light_accessories_headband = model.loupe_light_accesories_headband ?? "",
                loupe_light_accessories_connect = model.loupe_light_accesories_connect ?? "",
                lens_option_arcoating = model.lens_option ?? "",
                diamond_left = model.diamond_left ?? "",
                diamond_right = model.diamond_right ?? "",
                rebuild = model.rebuild,
                reuse_frame = model.reuse_frame,
                reuse_orcular = model.reuse_orcular,
                light_order_only = model.light_order_only,
                rebuild_notes = model.rebuild_notes ?? "",
                discount_code = model.discount_code ?? "",
                other_glasses_description = model.other_glasses_description ?? "",
                monovision_description = model.monovision_description ?? "",
                notes_for_the_presription = model.notes_for_the_presription ?? "",
                pd_inf_other_distance = model.pd_inf_other_distance ?? "",
                pd_inf_other_dist_right = model.pd_inf_other_dist_right ?? "",
                pd_inf_other_dist_left = model.pd_inf_other_dist_left ?? "",
                pd_inf_other_dist_total = model.pd_inf_other_dist_total ?? "",
                nosepad_color = model.nosepad_color ?? "",
                picture_upload_data_field = model.picture_upload_data_field ?? "",
                pd_notes = model.pd_notes ?? "",
                streetname = model.streetname ?? "",
                city_zip = model.city_zip ?? "",
                country_State_Region = model.country_State_Region ?? "",
                //Check med Tvede om det er korrekt
                latest_eye_test = model.latest_eye_test.Parse(1960),
                test_orderscol = "Testorder",
                loupe_light_accessories_charger = model.loupe_light_accessories_charger ?? "",

                carrierLens_notes = model.carrierLens_notes ?? "",
                frameFitting_notes = model.frameFitting_notes ?? ""

            };
            return result;
        }

        
    }
}