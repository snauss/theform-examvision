﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ExamVision___Form.TransferMethod
{
    [Table("test_orders")]
    public class TransferModel
	{
        [Key]
        public Int32 id { get; set; }

        public string comments { get; set; }
        public string dealer_name { get; set; }
        public string dealer_email { get; set; }
        public string dealer_reference_number { get; set; }
        public string client_name { get; set; }
        public string client_address { get; set; }
        public string client_address2 { get; set; }
        public string client_phonefax { get; set; }

	    public Int32 client_birth { get; set; }
        public string client_occupation { get; set; }
        public string client_use_optics_kind { get; set; }
        public string client_wear_glasses { get; set; }
        public string right_sph { get; set; }
        public string right_cyl { get; set; }
        public string right_axis { get; set; }
        public string right_add { get; set; }
        public string right_prism { get; set; }
        public string right_base { get; set; }
        public string right_va { get; set; }
        public string left_sph { get; set; }
        public string left_cyl { get; set; }
        public string left_axis { get; set; }
        public string left_add { get; set; }
        public string left_prism { get; set; }
        public string left_base { get; set; }
        public string left_va { get; set; }
        public string present_working_range { get; set; }
        public string preferred_working_range { get; set; }
        public string loupe_magnification_type { get; set; }
        public string loupe_frame_type { get; set; }
        public string loupe_frame_size { get; set; }
        public string loupe_frame_colour { get; set; }
        public string loupe_angle { get; set; }
        public string lens_options { get; set; }
        public string loupe_light_system { get; set; }
        public string loupe_light_other_system { get; set; }
        public string ipd_inf_right { get; set; }
        public string ipd_inf_total { get; set; }
        public string ipd_inf_left { get; set; }
        public string ipd_40cm_right { get; set; }
        public string ipd_40cm_total { get; set; }
        public string ipd_40cm_left { get; set; }
        public string ipd_30cm_right { get; set; }
        public string ipd_30cm_total { get; set; }
        public string ipd_30cm_left { get; set; }
        public string ipd_height_right { get; set; }
        public string ipd_height_left { get; set; }
        public string ipd_vertex_right { get; set; }
        public string ipd_vertex_left { get; set; }
        public string setup_demo_right { get; set; }
        public string setup_demo_left { get; set; }
        public string control_distances_from { get; set; }
        public string control_distances_to { get; set; }
        public string outer_lenses_type { get; set; }
        public string monofocal_distance { get; set; }
        public string bifocal_version { get; set; }
        public string bifocal_distant { get; set; }
        public string bifocal_near { get; set; }
        public string bifocal_type { get; set; }
        public string outer_lenses_prescription_right_sph { get; set; }
        public string outer_lenses_prescription_right_cyl { get; set; }
        public string outer_lenses_prescription_right_axis { get; set; }
        public string outer_lenses_prescription_right_add { get; set; }
        public string outer_lenses_prescription_right_prism { get; set; }
        public string outer_lenses_prescription_right_base { get; set; }
        public string outer_lenses_prescription_right_va { get; set; }
        public string outer_lenses_prescription_left_sph { get; set; }
        public string outer_lenses_prescription_left_cyl { get; set; }
        public string outer_lenses_prescription_left_axis { get; set; }
        public string outer_lenses_prescription_left_add { get; set; }
        public string outer_lenses_prescription_left_prism { get; set; }
        public string outer_lenses_prescription_left_base { get; set; }
        public string outer_lenses_prescription_left_va { get; set; }
        public string frame_temple_r { get; set; }
        public string frame_temple_l { get; set; }
        public string frame_width { get; set; }
        public string frame_nose_ridge { get; set; }
        public string frame_engraved_name { get; set; }
        public string client_use_optics { get; set; }
        public string monofocal_type { get; set; }
        public string nav_id { get; set; }
        public string item_no_plano { get; set; }
        public string item_no_prescription { get; set; }
        public string item_no_frame { get; set; }
        public string item_no_light { get; set; }
        public Int32 flag { get; set; }
        public string loupe_magnification { get; set; }
        public string loupe_accesories_vshield_small { get; set; }
        public string loupe_attachment { get; set; }
        public string loupe_accesories_vshield_large { get; set; }
        public string loupe_accesories_curing_filter { get; set; }
        public string progressive_version { get; set; }
        public string dealer_alternative_email { get; set; }
        public string lens_option_prisma { get; set; }
        public string lens_option_shv { get; set; }
        public Int32 fake { get; set; }
        public string order_button { get; set; }
        public string cosmo_deep_version { get; set; }
        public string salespersons_name { get; set; }
        public string loupe_angle_other { get; set; }
        public string sideshields { get; set; }
        public string nose_pads { get; set; }
        public string generate { get; set; }
        public string suitcase { get; set; }
        public string lens_option_aircoating { get; set; }
        public string light_system { get; set; }
        public string loupe_light_control_unit { get; set; }
        public string loupe_light_accessories_curing { get; set; }
        public string loupe_light_accessories_handsfree { get; set; }
        public string loupe_accesories_sideshield { get; set; }
        public string loupe_light_accessories_clip { get; set; }
        public string control_distances_from_left { get; set; }
        public string control_distances_to_left { get; set; }
        public string control_distances_from_right { get; set; }
        public string control_distances_to_right { get; set; }
        public string loupe_light_accessories_headband { get; set; }
        public string loupe_light_accessories_connect { get; set; }
        public string lens_option_arcoating { get; set; }
        public string diamond_left { get; set; }
        public string diamond_right { get; set; }
        public Int32 rebuild { get; set; }
        public Int32 reuse_frame { get; set; }
        public Int32 reuse_orcular { get; set; }
        public Int32 light_order_only { get; set; }
        public string rebuild_notes { get; set; }
        public string discount_code { get; set; }
        public string other_glasses_description { get; set; }
        public string monovision_description { get; set; }
        public string notes_for_the_presription { get; set; }
        public string pd_inf_other_distance { get; set; }
        public string pd_inf_other_dist_right { get; set; }
        public string pd_inf_other_dist_left { get; set; }
        public string pd_inf_other_dist_total { get; set; }
        public string nosepad_color { get; set; }
        public string picture_upload_data_field { get; set; }
        public string pd_notes { get; set; }
        public string streetname { get; set; }
        public string city_zip { get; set; }
        public string country_State_Region { get; set; }
        public Int32 latest_eye_test { get; set; }
        public string test_orderscol { get; set; }
        public string carrierLens_notes { get; set; }
        public string frameFitting_notes { get; set; }
        public string loupe_light_accessories_charger { get; set; }
    }
}