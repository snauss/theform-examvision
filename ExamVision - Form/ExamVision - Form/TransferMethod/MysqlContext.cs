﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MySql.Data.Entity;

namespace ExamVision___Form.TransferMethod
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class MysqlContext : DbContext
    {
        public MysqlContext() : base("MysqlFirst")
        {
            Database.SetInitializer<MysqlContext>(null);
        }
        public DbSet<TransferModel> test_orders { get; set; }
         

    }
}