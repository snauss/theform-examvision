namespace ExamVision___Form.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class notesNow120Chars : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FormModels", "rebuild_notes", c => c.String(maxLength: 120));
            AlterColumn("dbo.FormModels", "notes_for_the_presription", c => c.String(maxLength: 120));
            AlterColumn("dbo.FormModels", "pd_notes", c => c.String(maxLength: 120));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FormModels", "pd_notes", c => c.String(maxLength: 45));
            AlterColumn("dbo.FormModels", "notes_for_the_presription", c => c.String(maxLength: 45));
            AlterColumn("dbo.FormModels", "rebuild_notes", c => c.String(maxLength: 45));
        }
    }
}
