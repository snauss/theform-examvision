namespace ExamVision___Form.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class firstGoAfterUpdate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FormModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        comments = c.String(maxLength: 245),
                        dealer_name = c.String(nullable: false, maxLength: 30),
                        dealer_mail = c.String(nullable: false, maxLength: 80),
                        dealer_reference_number = c.String(maxLength: 50),
                        client_name = c.String(maxLength: 50),
                        client_address = c.String(maxLength: 50),
                        client_address2 = c.String(maxLength: 50),
                        client_phonefax = c.String(maxLength: 30),
                        client_birth = c.Int(nullable: false),
                        client_occupation = c.String(maxLength: 50),
                        client_use_optics_kind = c.String(maxLength: 80),
                        client_wear_glasses = c.String(maxLength: 50),
                        right_sph = c.String(maxLength: 10),
                        right_cyl = c.String(maxLength: 10),
                        right_axis = c.String(maxLength: 10),
                        right_add = c.String(maxLength: 10),
                        right_prism = c.String(maxLength: 10),
                        right_base = c.String(maxLength: 10),
                        right_va = c.String(maxLength: 10),
                        left_sph = c.String(maxLength: 10),
                        left_cyl = c.String(maxLength: 10),
                        left_axis = c.String(maxLength: 10),
                        left_add = c.String(maxLength: 10),
                        left_prism = c.String(maxLength: 10),
                        left_base = c.String(maxLength: 10),
                        left_va = c.String(maxLength: 10),
                        present_working_range = c.String(maxLength: 10),
                        preferred_working_range = c.String(maxLength: 10),
                        loupe_magnification_type = c.String(maxLength: 20),
                        loupe_frame_type = c.String(maxLength: 20),
                        loupe_frame_size = c.String(maxLength: 20),
                        loupe_frame_colour = c.String(maxLength: 20),
                        loupe_angle = c.String(maxLength: 20),
                        lens_options = c.String(maxLength: 20),
                        loupe_light_system = c.String(maxLength: 20),
                        loupe_light_other_system = c.String(maxLength: 20),
                        ipd_inf_right = c.String(maxLength: 20),
                        ipd_inf_total = c.String(maxLength: 20),
                        ipd_inf_left = c.String(maxLength: 20),
                        ipd_40cm_right = c.String(maxLength: 20),
                        ipd_40cm_total = c.String(maxLength: 20),
                        ipd_40cm_left = c.String(maxLength: 20),
                        ipd_30cm_right = c.String(maxLength: 20),
                        ipd_30cm_total = c.String(maxLength: 20),
                        ipd_30cm_left = c.String(maxLength: 20),
                        ipd_height_right = c.String(maxLength: 20),
                        ipd_height_left = c.String(maxLength: 20),
                        ipd_vertex_right = c.String(maxLength: 20),
                        ipd_vertex_left = c.String(maxLength: 20),
                        setup_demo_right = c.String(maxLength: 20),
                        setup_demo_left = c.String(maxLength: 20),
                        control_distances_from = c.String(maxLength: 20),
                        control_distances_to = c.String(maxLength: 20),
                        outer_lenses_type = c.String(maxLength: 20),
                        monofocal_distance = c.String(maxLength: 20),
                        bifocal_version = c.String(maxLength: 20),
                        bifocal_distant = c.String(maxLength: 20),
                        bifocal_near = c.String(maxLength: 20),
                        bifocal_type = c.String(maxLength: 30),
                        out_lens_prescrip_right_sph = c.String(maxLength: 30),
                        out_lens_prescrip_right_cyl = c.String(maxLength: 30),
                        out_lens_prescrip_right_axis = c.String(maxLength: 30),
                        out_lens_prescrip_right_add = c.String(maxLength: 30),
                        out_lens_prescrip_right_prism = c.String(maxLength: 30),
                        out_lens_prescrip_right_base = c.String(maxLength: 30),
                        out_lens_prescrip_right_va = c.String(maxLength: 30),
                        out_len_prescrip_left_sph = c.String(maxLength: 30),
                        out_lens_prescrip_left_cyl = c.String(maxLength: 30),
                        out_lens_prescrip_left_axis = c.String(maxLength: 30),
                        out_lens_prescrip_left_add = c.String(maxLength: 30),
                        out_lens_prescrip_left_prism = c.String(maxLength: 30),
                        out_lens_prescrip_left_base = c.String(maxLength: 30),
                        out_lens_prescrip_left_va = c.String(maxLength: 30),
                        frame_temple_r = c.String(maxLength: 30),
                        frame_temple_l = c.String(maxLength: 30),
                        frame_width = c.String(maxLength: 30),
                        frame_nose_ridge = c.String(maxLength: 30),
                        frame_engraved_name = c.String(maxLength: 30),
                        client_use_optics = c.String(maxLength: 30),
                        monofocal_type = c.String(maxLength: 30),
                        nav_id = c.String(maxLength: 30),
                        item_no_plano = c.String(maxLength: 30),
                        item_no_prescription = c.String(maxLength: 30),
                        item_no_frame = c.String(maxLength: 30),
                        item_no_light = c.String(maxLength: 30),
                        flag = c.Int(nullable: false),
                        loupe_magnification = c.String(maxLength: 30),
                        loupe_acce_vshield_small = c.String(maxLength: 30),
                        loupe_attachment = c.String(maxLength: 30),
                        loupe_acce_vshield_large = c.String(maxLength: 30),
                        loupe_acce_curing_filter = c.String(maxLength: 30),
                        progressive_version = c.String(maxLength: 30),
                        dealer_alternative_email = c.String(maxLength: 30),
                        lens_option_prisma = c.String(maxLength: 30),
                        lens_option_shv = c.String(maxLength: 30),
                        fake = c.String(maxLength: 30),
                        order_button = c.String(maxLength: 30),
                        cosmo_deep_version = c.String(maxLength: 30),
                        salespersons_name = c.String(maxLength: 30),
                        loupe_angle_other = c.String(maxLength: 30),
                        sideshields = c.String(maxLength: 30),
                        nose_pads = c.String(maxLength: 30),
                        generate = c.String(maxLength: 30),
                        suitcase = c.String(maxLength: 30),
                        lens_option_aircoating = c.String(maxLength: 30),
                        light_system = c.String(maxLength: 30),
                        loupe_light_control_unit = c.String(maxLength: 30),
                        loupe_light_accesories_curing = c.String(maxLength: 30),
                        loupe_light_accesories_handsfree = c.String(maxLength: 30),
                        loupe_accesories_sideshield = c.String(maxLength: 30),
                        loupe_light_accesories_clip = c.String(maxLength: 30),
                        control_distances_from_left = c.String(maxLength: 15),
                        control_distances_to_left = c.String(maxLength: 15),
                        control_distances_from_right = c.String(maxLength: 15),
                        control_distances_to_right = c.String(maxLength: 15),
                        loupe_light_accesories_headband = c.String(maxLength: 15),
                        loupe_light_accesories_connect = c.String(maxLength: 15),
                        lens_option = c.String(maxLength: 15),
                        diamond_left = c.String(maxLength: 15),
                        diamond_right = c.String(maxLength: 15),
                        rebuild = c.Int(nullable: false),
                        reuse_frame = c.Int(nullable: false),
                        reuse_orcular = c.Int(nullable: false),
                        light_order_only = c.Int(nullable: false),
                        rebuild_notes = c.String(maxLength: 45),
                        discount_code = c.String(maxLength: 45),
                        other_glasses_description = c.String(maxLength: 45),
                        monovision_description = c.String(maxLength: 45),
                        notes_for_the_presription = c.String(maxLength: 45),
                        pd_inf_other_distance = c.String(maxLength: 45),
                        pd_inf_other_dist_right = c.String(maxLength: 45),
                        pd_inf_other_dist_left = c.String(maxLength: 45),
                        pd_inf_other_dist_total = c.String(maxLength: 45),
                        nosepad_color = c.String(maxLength: 45),
                        picture_upload_data_field = c.String(maxLength: 45),
                        pd_notes = c.String(maxLength: 45),
                        streetname = c.String(maxLength: 45),
                        city_zip = c.String(maxLength: 45),
                        country_State_Region = c.String(maxLength: 45),
                        latest_eye_test = c.String(maxLength: 45),
                        test_orderscol = c.String(maxLength: 45),
                        isOrdered = c.Boolean(nullable: false),
                        loupe_light_accesories_charger = c.String(),
                        lens_option_arcoating = c.String(),
                        ImportDate = c.DateTime(nullable: false),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Glasses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Small = c.Boolean(nullable: false),
                        Medium = c.Boolean(nullable: false),
                        Large = c.Boolean(nullable: false),
                        Grey = c.Boolean(nullable: false),
                        Red = c.Boolean(nullable: false),
                        Black = c.Boolean(nullable: false),
                        Raw = c.Boolean(nullable: false),
                        Silver = c.Boolean(nullable: false),
                        DiamondLeft = c.Boolean(nullable: false),
                        DiamondRight = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.FormModels", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.FormModels", new[] { "User_Id" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Glasses");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.FormModels");
        }
    }
}
