namespace ExamVision___Form.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class clien_birthToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FormModels", "client_birth", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FormModels", "client_birth", c => c.Int(nullable: false));
        }
    }
}
