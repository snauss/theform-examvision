namespace ExamVision___Form.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixedCheckboxBool : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormModels", "rebuildToBool", c => c.Boolean(nullable: false));
            AddColumn("dbo.FormModels", "reuse_frameToBool", c => c.Boolean(nullable: false));
            AddColumn("dbo.FormModels", "reuse_orcularToBool", c => c.Boolean(nullable: false));
            AddColumn("dbo.FormModels", "light_order_onlyToBool", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormModels", "light_order_onlyToBool");
            DropColumn("dbo.FormModels", "reuse_orcularToBool");
            DropColumn("dbo.FormModels", "reuse_frameToBool");
            DropColumn("dbo.FormModels", "rebuildToBool");
        }
    }
}
