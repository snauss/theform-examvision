namespace ExamVision___Form.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lastLoginLengthAdded : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FormModels", "usersLastLogin", c => c.String(maxLength: 25));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FormModels", "usersLastLogin", c => c.String());
        }
    }
}
