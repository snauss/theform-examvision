namespace ExamVision___Form.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class descNow120Chars : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FormModels", "other_glasses_description", c => c.String(maxLength: 120));
            AlterColumn("dbo.FormModels", "monovision_description", c => c.String(maxLength: 120));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FormModels", "monovision_description", c => c.String(maxLength: 45));
            AlterColumn("dbo.FormModels", "other_glasses_description", c => c.String(maxLength: 45));
        }
    }
}
