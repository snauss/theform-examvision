namespace ExamVision___Form.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class clientCarrierLensVersionAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormModels", "clientCarrierLensVersion", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormModels", "clientCarrierLensVersion");
        }
    }
}
