namespace ExamVision___Form.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class framefittingcarrierLensNotesAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormModels", "carrierLens_notes", c => c.String(maxLength: 120));
            AddColumn("dbo.FormModels", "frameFitting_notes", c => c.String(maxLength: 120));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormModels", "frameFitting_notes");
            DropColumn("dbo.FormModels", "carrierLens_notes");
        }
    }
}
