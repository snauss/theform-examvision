namespace ExamVision___Form.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addressFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormModels", "clientCity", c => c.String());
            AddColumn("dbo.FormModels", "clientZip", c => c.String());
            AddColumn("dbo.FormModels", "clientRegion", c => c.String());
            AddColumn("dbo.FormModels", "clientCountry", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormModels", "clientCountry");
            DropColumn("dbo.FormModels", "clientRegion");
            DropColumn("dbo.FormModels", "clientZip");
            DropColumn("dbo.FormModels", "clientCity");
        }
    }
}
