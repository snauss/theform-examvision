namespace ExamVision___Form.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lastLoginFieldAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormModels", "usersLastLogin", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormModels", "usersLastLogin");
        }
    }
}
