namespace ExamVision___Form.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class loupe_light_accessories_chargerNameChanged : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormModels", "loupe_light_accessories_charger", c => c.String());
            DropColumn("dbo.FormModels", "loupe_light_accesories_charger");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FormModels", "loupe_light_accesories_charger", c => c.String());
            DropColumn("dbo.FormModels", "loupe_light_accessories_charger");
        }
    }
}
