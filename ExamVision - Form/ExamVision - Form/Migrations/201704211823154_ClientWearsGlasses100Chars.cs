namespace ExamVision___Form.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClientWearsGlasses100Chars : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FormModels", "client_wear_glasses", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FormModels", "client_wear_glasses", c => c.String(maxLength: 50));
        }
    }
}
