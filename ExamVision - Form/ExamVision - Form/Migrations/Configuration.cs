using System.Security.Cryptography.X509Certificates;
using ExamVision___Form.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ExamVision___Form.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ExamVision___Form.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
        
        protected override void Seed(ExamVision___Form.Models.ApplicationDbContext context)
        {
            if (!context.Roles.Any(r => r.Name == "Admin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Admin" };

                manager.Create(role);
            }

            if (!context.Users.Any(u => u.UserName == "Admin"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "Admin" };

                manager.Create(user, "adminpass");
                manager.AddToRole(user.Id, "Admin");
            }

           if (!context.Users.Any(u => u.UserName == "User"))
           {
               var store = new UserStore<ApplicationUser>(context);
               var manager = new UserManager<ApplicationUser>(store);
               var user = new ApplicationUser { UserName = "User" };

               manager.Create(user, "userpass");
           }

            context.Glasses.AddOrUpdate(
                x => x.Name,
                new Glasses
                {
                    Name = "Cosmo",
                    Small = false,
                    Medium = false,
                    Large = false,
                    Grey = false,
                    Red = false,
                    Black = false,
                    Raw = false,
                    Silver = false,
                    DiamondLeft = false,
                    DiamondRight = false
                },
                new Glasses
                {
                    Name = "Sport",
                    Small = false,
                    Medium = false,
                    Large = false,
                    Grey = false,
                    Red = false,
                    Black = false,
                    Raw = false,
                    Silver = false,
                    DiamondLeft = false,
                    DiamondRight = false
                },
                new Glasses
                {
                    Name = "Icon",
                    Small = false,
                    Medium = false,
                    Large = false,
                    Grey = false,
                    Red = false,
                    Black = false,
                    Raw = false,
                    Silver = false,
                    DiamondLeft = false,
                    DiamondRight = false
                },
                new Glasses
                {
                    Name = "Essential",
                    Small = false,
                    Medium = false,
                    Large = false,
                    Grey = false,
                    Red = false,
                    Black = false,
                    Raw = false,
                    Silver = false,
                    DiamondLeft = false,
                    DiamondRight = false
                }

                );

        }
    }
}
