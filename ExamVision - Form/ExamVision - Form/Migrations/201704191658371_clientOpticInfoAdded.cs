namespace ExamVision___Form.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class clientOpticInfoAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormModels", "ClientDoWearGlasses", c => c.String());
            AddColumn("dbo.FormModels", "ClientOpticForm", c => c.String());
            AddColumn("dbo.FormModels", "ClientContactType", c => c.String());
            AddColumn("dbo.FormModels", "ClientGlassType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormModels", "ClientGlassType");
            DropColumn("dbo.FormModels", "ClientContactType");
            DropColumn("dbo.FormModels", "ClientOpticForm");
            DropColumn("dbo.FormModels", "ClientDoWearGlasses");
        }
    }
}
