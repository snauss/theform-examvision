namespace ExamVision___Form.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class client_nameMadeRequired : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FormModels", "client_name", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FormModels", "client_name", c => c.String(maxLength: 50));
        }
    }
}
